import React from 'react';
import Login from "./component/Login"
import List from "./component/List"
import Detail from "./component/Detail"
import './App.css';
import { connect } from 'react-redux';
import {
  BrowserRouter as Router,
  Switch,
  Route,
  Redirect
} from "react-router-dom";

function App( props) {
  return (
    <Router>
      <Switch>
            <Route  exact path="/" > 
             { props.isLogin  ? <Redirect to="/List" /> : <Login /> }
            </Route>
            <Route  exact path="/List" > 
              <List/>
            </Route>
            <Route  path="/Detail"> 
              <Detail/>
            </Route>
      </Switch>
    </Router>   
  );
}

const mapStateToProps = (state) => ({
  isLogin: state.Login.isLogin,
  token: state.Login.token,
});

export default connect(mapStateToProps, null) (App);
