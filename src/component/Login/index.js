import React, {useState}from 'react';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import actions from './redux/action';

function Login(props) {

const [userName, updateUserName] = useState("");
const [password, updatePassword]= useState("");

      return (<div className="App">
      <header className="App-header">
        <input 
            type="text"
            name="userName" 
            value={userName} 
            onChange={(e)=>{
              updateUserName(e.target.value);
            }}
            placeholder="Enter User Name "/>
        <input 
            type="password"
             name="Password"
             value={password}  
             onChange={(e)=>{
                updatePassword(e.target.value);
              }}
             placeholder="Enter password "/>
        <button onClick={()=> props.login(userName,userName)}> submit </button>
      </header>
    </div>
  )
    
}

const mapStateToProps = (state) => ({
  isLogin: state.Login.isLogin,
});


const mapDispatchToProps = dispatch => bindActionCreators(Object.assign({}, actions), dispatch);

export default connect(mapStateToProps, mapDispatchToProps)((Login)); ;