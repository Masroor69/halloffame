import * as types from './Type';

export default function reducer(state = {
  token : null,
  isLogin : false,
}, action) {
  switch (action.type) {
    case types.LOGIN:
      return [
        ...state,
        action.payload
      ];
    case types.IS_LOGIN:
      return [
        ...state,
        action.payload
      ];
      case types.IS_SUCC:
        return{
          ...state,
          isLogin : !state.isLogin,
          token: action.payload,
        }
    default:
      return state;
  }
}
