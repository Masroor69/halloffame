import * as types from './Type';
import api from "./../../config/api"

export const login = (username, password ) => async (dispatch) => {
  api.post('/login',{ username, password })
  .then((response) => {
    if(response.data.data.success){
      const {authorization} = response.headers;
      dispatch({
      type: types.IS_SUCC,
      payload: authorization,
    });
    }
  }).catch(error => {
    console.log(error.message);
  });
};

export const LogOut = (errorIndex) => async (dispatch) => {
  dispatch({
    type: types.LOGOUT,
    payload: false,
  });
};

export default {
  login,
  LogOut,
};
