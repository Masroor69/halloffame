import React from "react";
import "./style.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import actions from "./redux/action";
import {
  Link,
} from "react-router-dom";
class List extends React.Component {
  componentDidMount() {
    console.log("token >>>>>>", this.props.token);
    this.props.fetchFames(0, this.props.token);
  }
  
  render() {
    console.log("this.props", this.props);
    const { isLoading, fames } = this.props;
    return (
      <div>
        <nav>
          <div className="header">
            <div>
              <h3>Fame List</h3>
            </div>
            <div>
              <button>LogOut</button>
            </div>
          </div>
        </nav>
        {isLoading && <p>Loading ..... </p>}
        {!isLoading && fames.length > 0 && (
          <table className="tg">
            <thead>
                <tr>
                    <th className="tg-cly1"></th>
                    <th className="tg-0lax">Name</th>
                    <th className="tg-0lax">Data Of Birth</th>
                    <th className="tg-0lax">Action</th>
                </tr>
            </thead>
            <tbody>
              {fames.map((fame, index) => (
                <tr key={index}>
                  <th className="tg-cly1">
                  {
                        
                  }
                    <img className={"image"} src={fame.image} alt={fame.name} />
                  </th>
                  <th className="tg-0lax">{fame.name}</th>
                  <th className="tg-0lax">{fame.dob}</th>
                  <th className="tg-0lax">
                    {" "}
                    <Link href={`./detail?id=${fame.id}`}> edit</Link>
                  </th>
                </tr>
              ))}
            </tbody>
          </table>
        )}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.Login.token,
  isLoading: state.List.isLoading,
  fames: state.List.fames
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(Object.assign({}, actions), dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(List);
