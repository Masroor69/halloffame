import * as types from './Type';

export default function reducer(state = {
  isLoading : true,
  fames : {},
}, action) {
  switch (action.type) {
    case types.FETCHING_FAME:
      return {
        ...state,
        fames: action.payload,
        isLoading: false
      }
    default:
      return state;
  }
}
