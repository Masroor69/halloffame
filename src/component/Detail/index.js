import React from "react";
import "./style.css";
import { bindActionCreators } from "redux";
import { connect } from "react-redux";
import actions from "./redux/action";

class Detail extends React.Component {
  componentDidMount() {
  }
  
  render() {
    console.log("this.props", this.props);
    const { isLoading } = this.props;
    return (
      <div>
        <nav>
          <div className="header">
            <div>
              <h3>Fame List</h3>
            </div>
            <div>
              <button>LogOut</button>
            </div>
          </div>
        </nav>
        {isLoading && <p>Loading ..... </p>}
      </div>
    );
  }
}

const mapStateToProps = state => ({
  token: state.Login.token,
  isLoading: state.List.isLoading,
  fames: state.List.fames
});

const mapDispatchToProps = dispatch =>
  bindActionCreators(Object.assign({}, actions), dispatch);

export default connect(mapStateToProps, mapDispatchToProps)(Detail);
