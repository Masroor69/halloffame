import * as types from './Type';
import api from "../../config/api"

export const fetchFames = ( page, token) => async (dispatch) => {
  if(token != null ){
    console.log("Token is " , token);
    api.get(`/fames?page=${page}`, {headers: {"Authorization":token}})
    .then((response) => {
      const {list} = response.data.data;
      dispatch({
        type: types.FETCHING_FAME,
        payload: list,
      });
      
      }
    ).catch(error => {
      console.log(error.message);
    });
  }if (token === null){
    console.log("Token is Null");
    api.get(`/fames?page=${page}&guest=true`)
    .then((response) => {
      const {list} = response.data.data;
      dispatch({
        type: types.FETCHING_FAME,
        payload: list,
      });
      
      }
    ).catch(error => {
      console.log(error.message);
    });
  }
};



export default {
  fetchFames
};
