import axios from 'axios';

export default axios.create({
  baseURL: `https://halloffame-server.herokuapp.com/`
});