import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import reducers from './Reducers';
import DevTools from './DevTools';

const enhancer = compose(
  applyMiddleware(thunk),
  DevTools.instrument()
);

export default function configureStore(initialState) {
  const store = createStore(reducers, initialState, enhancer);
  if (module.hot) {
    module.hot.accept('./Reducers', () =>
      store.replaceReducer(
        require('./Reducers') 
      )
    );
  }

  return store;
}