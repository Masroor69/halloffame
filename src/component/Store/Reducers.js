import { combineReducers } from 'redux';
import GeneralReducers from '../redux/reducer';
import LoginReducers from '../Login/redux/reducer';
import fameReducers from '../List/redux/reducer'
import detailReducers from '../Detail/redux/reducer'
export default combineReducers({
  General: GeneralReducers,
  Login : LoginReducers,
  List:fameReducers,
  Detail:detailReducers
});
