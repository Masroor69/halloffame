import * as types from './Type';

export const addingUpdate = () => async (dispatch) => {
  dispatch({
    type: types.UPDATE_DATA,
  });
};

export default {
  addingUpdate,
};
