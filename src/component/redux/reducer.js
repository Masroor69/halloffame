import * as types from './Type';

export default function reducer(state = {
  update: 0,
}, action) {
  switch (action.type) {
    case types.UPDATE_DATA:
      return {
        ...state,
        update: state.update + 1
      };
    default:
      return state;
  }
}
